package me.jakubko.main;

import me.jakubko.main.hashes.Base64Decode;
import me.jakubko.main.hashes.MD5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.Base64;

/**
 * @author jakuubkoo
 * @since 06/09/2019
 */
public class Main {

    public static String decode = null;
    public static String encode = null;

    public static void main(String[] args){
        JFrame jFrame = new JFrame("Encryptor and Decryptor | Desktop Version | by jakuubkoo#0002");
        jFrame.setResizable(false);
        jFrame.setSize(520, 410);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.getContentPane().setLayout(null);

        //Center of screen maker
        Toolkit tk = jFrame.getToolkit();
        Dimension dim = tk.getScreenSize();
        int x = (int) dim.getWidth() / 2 - jFrame.getWidth() / 2;
        int y = (int) dim.getHeight() / 2 - jFrame.getHeight() / 2;
        jFrame.setLocation(x, y);

        JComboBox jComboBox = new JComboBox();
        jComboBox.setBounds(50, 35, 280, 35);
        jComboBox.addItem("Base64");
        jComboBox.addItem("MD5");
        jComboBox.setSelectedItem("Base64");
        jComboBox.setVisible(true);

        String value = (String) jComboBox.getSelectedItem();

        JLabel decryptText2 = new JLabel("Decrypt");
        decryptText2.setVisible(true);
        decryptText2.setBounds(50, 100, 280, 35);

        JTextField decryptText = new JTextField();
        decryptText.setBounds(50, 130, 280, 35);

        JLabel encryptText2 = new JLabel("Encrypt");
        encryptText2.setVisible(true);
        encryptText2.setBounds(50, 200, 280, 35);


        JTextField encryptText = new JTextField();
        encryptText.setBounds(50, 230, 280, 35);

        JButton jButton = new JButton();
        jButton.setText("Decrypt");
        jButton.setBounds(50, 330, 90, 35);
        jButton.setVisible(true);

        JButton jButton2 = new JButton();
        jButton2.setText("Encrypt");
        jButton2.setBounds(180, 330, 90, 35);
        jButton2.setVisible(true);

        jButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (encryptText.getText() != null) {
                    encode = encryptText.getText();
                    if (jComboBox.getSelectedItem().toString().equalsIgnoreCase("Base64")){
                        String encoded = Base64Decode.encode(encode.getBytes());
                        decryptText.setText(encoded);
                    }else if (jComboBox.getSelectedItem().toString().equalsIgnoreCase("MD5")){
                        String encoded = null;
                        try {
                            encoded = MD5.convertToMD5(encode);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        decryptText.setText(encoded);
                    }else{
                        JOptionPane.showMessageDialog(jFrame,
                                "Empty box.",
                                "Error!",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }else{
                    encode = null;
                }
            }
        });

        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (decryptText.getText() != null) {
                    decode = decryptText.getText();
                    if (jComboBox.getSelectedItem().toString().equalsIgnoreCase("Base64")) {
                        byte[] decodedBytes = Base64.getDecoder().decode(decode);
                        String decodedString = new String(decodedBytes);
                        decryptText.setText(decodedString);
                    }else{
                        JOptionPane.showMessageDialog(jFrame,
                                "Empty box.",
                                "Error!",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }else{
                    decode = null;
                }
            }
        });

        jFrame.getContentPane().add(jComboBox);
        jFrame.getContentPane().add(decryptText);
        jFrame.getContentPane().add(decryptText2);
        jFrame.getContentPane().add(encryptText);
        jFrame.getContentPane().add(encryptText2);
        jFrame.getContentPane().add(jButton);
        jFrame.getContentPane().add(jButton2);

        jFrame.setVisible(true);
    }

}
